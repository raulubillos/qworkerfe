
import Login from './screens/Login';
import EStyleSheet from 'react-native-extended-stylesheet';
import { ApolloProvider } from '@apollo/client';
import { Client } from './apollo/client';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Register from './screens/Register';
import { Screens } from './shared/routes';

const Stack = createNativeStackNavigator<Screens>();

export default function App() {
  
  EStyleSheet.build({
    '@media (orientation: landscape)': {
      '$flexDirectionContainer':'row',
    },
    '@media (orientation: portrait)': {
      '$flexDirectionContainer':'column',
    }
  })
  return (
    <ApolloProvider client={Client}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='login'>
          <Stack.Screen name='login' component={Login} options={{headerShown:false}}/>
          <Stack.Screen name='register' component={Register} options={{headerShown:false}}/>
        </Stack.Navigator>
      </NavigationContainer>
    </ApolloProvider>
  );
}


