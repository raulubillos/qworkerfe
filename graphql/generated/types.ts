export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type loginWithCredentialInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type loginWithCredentialOutput = {
  __typename?: 'LoginWithCredentialOutput';
  access_token: Scalars['String'];
  apiKey: Scalars['String'];
};

export type mutation = {
  __typename?: 'Mutation';
  loginWithCredential: loginWithCredentialOutput;
  registerUserCredentials: registerUserCredentialsOutput;
};


export type mutationloginWithCredentialArgs = {
  credentials: loginWithCredentialInput;
};


export type mutationregisterUserCredentialsArgs = {
  credentials: registerUserCredentialsInput;
};

export type query = {
  __typename?: 'Query';
  sayHello: Scalars['String'];
};

export type registerUserCredentialsInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type registerUserCredentialsOutput = {
  __typename?: 'RegisterUserCredentialsOutput';
  apikey: Scalars['String'];
  user_id: Scalars['String'];
};
