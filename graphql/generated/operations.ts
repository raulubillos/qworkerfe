import * as Types from './types';

export type loginMutationVariables = Types.Exact<{
  credentials: Types.loginWithCredentialInput;
}>;


export type loginMutation = { __typename?: 'Mutation', loginWithCredential: { __typename?: 'LoginWithCredentialOutput', apiKey: string } };
