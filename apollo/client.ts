import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import environment from "../environment";

const loginLink = new HttpLink({
    uri:environment.LOGIN_URL
})

export const Client = new ApolloClient({
    link:loginLink,
    cache:new InMemoryCache()
})