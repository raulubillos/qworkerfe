

import { StyleProp } from "emotion-native-extended/dist/types/StyleSheet";
import React from "react";
import { ViewStyle } from "react-native";
import { SvgXml, XmlProps } from "react-native-svg"; 

interface Props {
  style:StyleProp<ViewStyle>
}

export default function PhotoIcon(props:Props){  
  const svgMarkup = `
  <svg width="90" height="90" viewBox="0 0 90 90" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M78.75 22.5H66.8625L60 15H37.5V22.5H56.7L63.5625 30H78.75V75H18.75V41.25H11.25V75C11.25 79.125 14.625 82.5 18.75 82.5H78.75C82.875 82.5 86.25 79.125 86.25 75V30C86.25 25.875 82.875 22.5 78.75 22.5ZM30 52.5C30 62.85 38.4 71.25 48.75 71.25C59.1 71.25 67.5 62.85 67.5 52.5C67.5 42.15 59.1 33.75 48.75 33.75C38.4 33.75 30 42.15 30 52.5ZM48.75 41.25C54.9375 41.25 60 46.3125 60 52.5C60 58.6875 54.9375 63.75 48.75 63.75C42.5625 63.75 37.5 58.6875 37.5 52.5C37.5 46.3125 42.5625 41.25 48.75 41.25ZM18.75 22.5H30V15H18.75V3.75H11.25V15H0V22.5H11.25V33.75H18.75V22.5Z" fill="black" fill-opacity="0.7"/>
  </svg>
  
  `;
  const SvgImage = () => <SvgXml style={props.style} xml={svgMarkup} />;  

  return <SvgImage />;
}