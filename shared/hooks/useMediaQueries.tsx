import "@expo/match-media";
import { useMediaQuery } from "react-responsive";

const useMediaQueries = () => {
    const tablet = useMediaQuery({
        minDeviceWidth:768,
        maxDeviceWidth:1024
    })

    const pc = useMediaQuery({
        minDeviceWidth:1024,
        maxDeviceWidth:1200
    })

    return {
        tablet,
        pc
    }
}

export default useMediaQueries;