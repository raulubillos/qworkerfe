import EStyleSheet from 'react-native-extended-stylesheet';
import { StatusBar, Platform } from 'react-native';

export const ContainerStyle = EStyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        justifyContent:'flex-end',
        flexDirection:'$flexDirectionContainer',
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
    },
    logo:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center'

    },
    form:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        paddingLeft:50,
        paddingRight:50
    }
})