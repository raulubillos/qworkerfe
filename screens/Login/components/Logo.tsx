import QLogo from '../../../assets/QLogo';

const Logo = () => {
    return <QLogo height={250} width={250}></QLogo>
}

export default Logo;