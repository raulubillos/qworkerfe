import { Pressable, Text, TextInput } from "react-native"
import { LoginFormStyle } from "../styles/Login"
import { useLogin } from "../../../api/login/hooks";
import { useForm, Controller } from "react-hook-form";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { Screens } from '../../../shared/routes';

interface Props{
    navigation:NativeStackNavigationProp<Screens, "register">
}

const LoginForm = ({navigation}:Props) => {
    const { control,handleSubmit, formState, getValues } = useForm({
        defaultValues:{
            username:'',
            password:''
        }
    });
    const {login, data, error, isLoading} = useLogin()
    
    const submitForm = async ({username,password}) => {
            try{
                await login({
                    variables:{
                        "credentials": {
                            "username": username,
                            "password": password
                        }
                    }
                })
            }catch(e){
                console.log(e)
            }
            
        
    }

    return <>
        <Controller 
            control={control}
            rules={{
                required:true,
                pattern: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
            }}
            name="username"
            render={({field: {onBlur, onChange, value}}) => {
                return <TextInput
                                onBlur={onBlur}
                                onChangeText={onChange}
                                value={value}
                                placeholder="Email" 
                                style={LoginFormStyle.input}
                        />
            }}
        />
        <Controller 
            control={control}
            rules={{
                required:true
            }}
            name="password"
            render={({field: {onBlur, onChange, value}}) => {
                return <TextInput
                                textContentType="password" 
                                secureTextEntry={true} 
                                onBlur={onBlur}
                                onChangeText={onChange}
                                value={value}
                                placeholder="Contraseña" 
                                style={LoginFormStyle.input}
                        />
            }}
        />
        <Pressable 
            onPress={handleSubmit(submitForm)}
            style={
                ({ pressed }) => [
                    {
                        backgroundColor: pressed?'#a8a8a8':'#F5F5F5'
                    },
                    LoginFormStyle.button
                ]
                }
        >
            <Text  style={LoginFormStyle.buttonTitle}>Iniciar sesion</Text>
        </Pressable>
        <Pressable
            onPress={() => {
                navigation.navigate('register');
            }}
            style={
            ({ pressed }) => [
                {
                    backgroundColor: pressed?'#a8a8a8':'#F5F5F5'
                },
                LoginFormStyle.button
            ]
            }>
            <Text  style={LoginFormStyle.buttonTitle}>Registrarme</Text>
        </Pressable>
        </>
}

export default LoginForm