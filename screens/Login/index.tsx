import React from "react"
import { View } from 'react-native';
import LoginForm from "./components/LoginForm";
import Logo from "./components/Logo";
import { ContainerStyle } from "../../shared/styles";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { Screens } from '../../shared/routes';

type Props = NativeStackScreenProps<Screens,'register'>

const Login = ({navigation}:Props) => {
    return <View style={ContainerStyle.container}>
        <View style={ContainerStyle.logo}>
            <Logo></Logo>
        </View>
        <View style={ContainerStyle.form}>
            <LoginForm navigation={navigation}></LoginForm>
        </View>
    </View>
}

export default Login