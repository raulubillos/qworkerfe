import EStyleSheet from 'react-native-extended-stylesheet';
import { StatusBar, Platform } from 'react-native';

export const LogoStyle = EStyleSheet.create({
    logo:{
        display: 'flex',
        flexDirection:'column',
        justifyContent:'flex-end',
        alignItems:'center',
        backgroundColor:'red'
    }
})

export const LoginFormStyle = EStyleSheet.create({
    input:{
        borderColor:'#adadb4',
        borderStyle:'solid',
        backgroundColor:'#FCFCFD',
        borderRadius:15,
        width: '100%',
        textAlign: 'center',
        height: 50,
        marginBottom:20,
    },
    button:{
        padding: 15,
        borderRadius:15,
        borderColor:'#DFDFE6',
        backgroundColor:'#FCFCFD',
        marginBottom:10
    },
    buttonTitle:{
        fontFamily:'Roboto', 
        fontSize:16, 
        fontWeight:'bold'
    }
})