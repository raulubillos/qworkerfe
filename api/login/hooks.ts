import { useLoginMutation } from "../../graphql/generated/hooks"

export const useLogin = () => {
    const [login, {called,loading,data,error}] = useLoginMutation();

    return {
        login,
        isLoading: called&&loading,
        error,
        data
    }
}